# Questionário para novos membros do ministério.

Para preencher o formulário faça um fork deste repositório, e submeta um pull request com suas alterações.

## Informações pessoais:

1. Qual o seu nome?
2. Qual sua idade?
3. Está há quanto tempo na Onda Dura?
4. Frequenta GP? Se sim, qual o nome do apascentador?
5. É discipulado? Se sim, qual o nome do discipulador?
6. Discipula alguém?
7. Serve em algum outro ministério?
8. O que te motivou a querer servir neste ministério?
9. Já programou para web?
10. Quais os seus conhecimentos na área? Quais linguagens, frameworks, ferramentas, etc.
11. Tem algum projeto ou código seu para nos apresentar?
12. Quanto tempo você disponibilizaria semanalmente para servir neste ministério? (Utilizaremos esse parâmetro para alocar atividades, portanto seja realista, não diga um valor o qual você não consegue cumprir, da mesma forma seja generoso, não ofereça apenas o que sobra)

## Desafio de lógica

### Objetivo:

O objetivo desse desafio é demonstrar sua experiência e conhecimento como programador, assim saberemos como você pensa e como resolve problemas na vida real.

### O Problema:
Escreva um programa onde, dado um determinado horário (hora/minutos) seja capaz de calcular o ângulo entre os 2 ponteiros do relógio.

### Requisitos do desafio:
- O movimento dos ponteiros ocorrem de minuto em minuto;
- O resultado não deverá ser recalculado caso a mesma consulta seja executada duas vezes;
- Deve ser retornado sempre o menor ângulo;

### Exemplo de teste:

```
Hora: 6, minuto: 0
Ângulo: 180
```
```
Hora: 3, minuto: 0
Ângulo: 90
```
```
Hora: 9, minuto: 0
Ângulo: 90
```

### Requisitos técnicos:
- Você pode usar a linguagem de programação que preferir;
- Utilize a plataforma que bem entender, mas como trabalharemos com uma aplicação web, seria mais interessante se pudesse nos mostrar isso;
- Se conseguir, gostaríamos de uma interface bacana;
- Você é livre, divirta-se.
